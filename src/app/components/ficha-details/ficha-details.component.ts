import { Component, OnInit } from '@angular/core';
import { FichaService } from 'src/app/services/ficha.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ficha-details',
  templateUrl: './ficha-details.component.html',
  styleUrls: ['./ficha-details.component.css']
})
export class FichaDetailsComponent implements OnInit {
  currentFicha = null;
  message = '';

  constructor(
    private fichaService: FichaService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getFicha(this.route.snapshot.paramMap.get('id'));
  }

  getFicha(id): void {
    this.fichaService.get(id)
      .subscribe(
        data => {
          this.currentFicha = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updateCompleted(status): void {
    const data = {
      full_name: this.currentFicha.full_name,
      enter_name: this.currentFicha.enter_name,
      email: this.currentFicha.email,
      pnumber: this.currentFicha.pnumber,
      category: this.currentFicha.category,
      note: this.currentFicha.note,
      completed: status
    };

    this.fichaService.update(this.currentFicha.id, data)
      .subscribe(
        response => {
          this.currentFicha.completed = status;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  updateFicha(): void {
    this.fichaService.update(this.currentFicha.id, this.currentFicha)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'La Ficha se actualizo con exito!';
        },
        error => {
          console.log(error);
        });
  }

  deleteFicha(): void {
    this.fichaService.delete(this.currentFicha.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/fichas']);
        },
        error => {
          console.log(error);
        });
  }
}
