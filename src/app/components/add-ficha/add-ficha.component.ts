import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FichaService } from 'src/app/services/ficha.service';

@Component({
  selector: 'app-add-ficha',
  templateUrl: './add-ficha.component.html',
  styleUrls: ['./add-ficha.component.css']
})

export class AddFichaComponent implements OnInit {
  ficha = {
    full_name: '',
    enter_name: '',
    email: '',
    pnumber: '',
    category: '',
    note: '',
    completed: false
  };
  submitted = false;

  constructor(private fichaService: FichaService) { }

  ngOnInit(): void {
  }

  saveFicha(): void {
    const data = {
      full_name: this.ficha.full_name,
      enter_name: this.ficha.enter_name,
      email: this.ficha.email,
      pnumber: this.ficha.pnumber,
      category: this.ficha.category,
      note: this.ficha.note,
    };

    this.fichaService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newFicha(): void {
    this.submitted = false;
    this.ficha = {
      full_name: '',
      enter_name: '',
      email: '',
      pnumber: '',
      category: '',
      note: '',
      completed: false
    };
  }

}
