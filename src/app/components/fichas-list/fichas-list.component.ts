import { Component, OnInit } from '@angular/core';
import { FichaService } from 'src/app/services/ficha.service';

@Component({
  selector: 'app-fichas-list',
  templateUrl: './fichas-list.component.html',
  styleUrls: ['./fichas-list.component.css']
})
export class FichasListComponent implements OnInit {

  fichas: any;
  currentFicha = null;
  currentIndex = -1;
  full_name = '';

  constructor(private fichaService: FichaService) { }

  ngOnInit(): void {
    this.retrieveFichas();
  }

  retrieveFichas(): void {
    this.fichaService.getAll()
      .subscribe(
        data => {
          this.fichas = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  refreshList(): void {
    this.retrieveFichas();
    this.currentFicha = null;
    this.currentIndex = -1;
  }

  setActiveFicha(ficha, index): void {
    this.currentFicha = ficha;
    this.currentIndex = index;
  }

  removeAllFichas(): void {
    this.fichaService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchFull_Name(): void {
    this.fichaService.findByFull_Name(this.full_name)
      .subscribe(
        data => {
          this.fichas = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
