import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddFichaComponent } from './components/add-ficha/add-ficha.component';
import { FichaDetailsComponent } from './components/ficha-details/ficha-details.component';
import { FichasListComponent } from './components/fichas-list/fichas-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AddFichaComponent,
    FichaDetailsComponent,
    FichasListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
