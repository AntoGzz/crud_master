import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FichasListComponent } from './components/fichas-list/fichas-list.component';
import { FichaDetailsComponent } from './components/ficha-details/ficha-details.component';
import { AddFichaComponent } from './components/add-ficha/add-ficha.component';


const routes: Routes = [
  { path: '', redirectTo: 'fichas', pathMatch: 'full' },
  { path: 'fichas', component: FichasListComponent },
  { path: 'fichas/:id', component: FichaDetailsComponent },
  { path: 'add', component: AddFichaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
